#include<iostream>
using namespace std;

struct Pair
{
  int x;
  int y;
};


ostream& operator<<(ostream& out, const Pair& p)
{
  out << "(" << p.x << "," << p.y << ")";
  return out;
}


void initialize(Pair* pairs, int size)
{
  for(int i = 0; i < size; i++)
  {
    pairs[i].x = i;
    pairs[i].y = size - i;
  }
}


void pretty_print(Pair* pairs, int size)
{
  cout << "[";
  int i = 0;

  for(; i < size - 1; i++)
  {
    cout << pairs[i] << ", ";
  }
  cout << pairs[i] << "]" << endl;
}

int main()
{
  Pair* pairs;// = new Pair[5];

  initialize(pairs, 5);

  pretty_print(pairs, 5);

  delete [] pairs;

  return 0;
}
