#include<iostream>
#include<iomanip>
using namespace std;


int main()
{

  /* An array of arrays! */

  int** array2d = new int*[5];
  for(int i = 0; i < 5; i++)
    array2d[i] = new int[5];

  for(int i = 0; i < 5; i++)
  {
    for(int j = 0; j < 5; j++)
    {
      array2d[i][j] = i*j;
      cout << setw(2) << array2d[i][j] << " ";
    }

    cout << endl;
  }

  return 0;
}