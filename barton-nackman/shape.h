#pragma once
#include<iostream>
using namespace std;

#include "clone.h"

class Shape
{
  public:
    virtual double area() const { return 0; }
    virtual double volume() const { return 0; }

    virtual void print() const = 0;

    virtual Shape* clone() const = 0;
    virtual ~Shape() {}

};

class Circle : public Clone<Shape, Circle>
{
  public:
    Circle(const double radius) : m_radius(radius) {}

    double radius() const { return m_radius; }

    void print() const { cout << "Circle with area " << area() << endl; }

    virtual double area() const { return 3.14 * m_radius * m_radius; }

  private:
    double m_radius;
};

class Square : public Clone<Shape, Square>
{
  public:
    Square(const double length) : m_length(length) {}

    double length() const { return m_length; }

    void print() const { cout << "Square with area " << area() << endl; }

    virtual double area() const { return m_length * m_length; }

  private:
    double m_length;
};
