#pragma once

#include "equal.h"

class Point: private Equal<Point>
{
  public:
    Point(const int x, const int y) : x(x), y(y) {}

    int x;
    int y;

    bool equalTo(const Point& p) const { return x == p.x &&  y == p.y; }
};
