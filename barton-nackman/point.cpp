#include<iostream>
using namespace std;

#include "point.h"

int main()
{
  Point p1(3,4);
  Point p2(5,6);
  Point p3(3,4);

  cout << "p1 == p2 " << (p1 == p2) << endl;
  cout << "p1 == p3 " << (p1 == p3) << endl;
  cout << "p1 != p2 " << (p1 != p2) << endl;

  return 0;
}

/* OUTPUT:
p1 == p2 0
p1 == p3 1
p1 != p2 1
*/
