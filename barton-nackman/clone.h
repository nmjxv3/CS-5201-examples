#pragma once

template<class Base, class Derived>
class Clone : virtual public Base
{
  public:
    Base* clone() const override
    {
      return new Derived(static_cast<const Derived&>(*this));
    }
};
