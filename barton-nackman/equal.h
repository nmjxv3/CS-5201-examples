#pragma once

template<class Derived>
class Equal
{
  public:
    friend bool operator==(const Derived& lhs, const Derived& rhs) { return lhs.equalTo(rhs); }
    friend bool operator!=(const Derived& lhs, const Derived& rhs) { return !lhs.equalTo(rhs); }
};

