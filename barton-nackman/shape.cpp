#include<iostream>
using namespace std;

#include "shape.h"

void dup_and_print(const Shape& shape)
{
  Shape* duplicate = shape.clone();

  duplicate->print();

  delete duplicate;
}

int main()
{
  Circle circle(2);
  Square square(2);

  dup_and_print(circle);
  dup_and_print(square);

  return 0;
}

/* OUTPUT:
Circle with area 12.56
Square with area 4
*/
