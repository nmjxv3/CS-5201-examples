#pragma once

#include "shape.h"

constexpr double PI = 3.141592653589793;

template<class Derived>
class Circular : public Shape<Derived>
{
  public:
    Circular(const double radius = 1, const int x = 0, const int y = 0) : m_radius(radius), Shape<Derived>(x,y) {}

    // Interface
    void setRadius(const double radius) { asDerived().setRadius_impl(radius); }

    double radius() const { return asDerived().radius_impl(); }


    // Implementation
    void setRadius_impl(const double radius) { m_radius = radius; }

    double radius_impl() const { return m_radius; }

    double area_impl() const { return PI * radius() * radius(); }


    // CRTP casts
    Derived& asDerived() { return static_cast<Derived&>(*this); }
    const Derived& asDerived() const { return static_cast<const Derived&>(*this); }

  protected:
    double m_radius;

};

