#pragma once

#include <string>
using std::string;
#include <iostream>
using std::cout;

#include "shape.h"

class Point : public Shape<Point>
{
  public:
    Point(const int x = 0, const int y = 0) : Shape(x,y) {}

    string name() const { return "Point"; }
    void print() const { cout << "Point: [" << x() << ", " << y() << "]\n"; }

};

