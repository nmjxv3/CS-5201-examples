#pragma once

#include <string>
using std::string;
#include <iostream>
using std::cout;

#include "circular.h"

class Cylinder : public Circular<Cylinder>
{
  public:
    Cylinder(const double height = 1, const double radius = 1, const int x = 0, const int y = 0) : m_height(height), Circular(radius, x, y) {}

    void setHeight(const double height) { m_height = height; }
    double height() const { return m_height; }

    double area_impl() const { return 2 * PI * radius() * height() + 2 * PI * radius() * radius(); }
    double volume_impl() const { return PI * radius() * radius() * height(); }

    string name() const { return "Cylinder"; }
    void print() const { cout << "Cylinder: [" << x() << ", " << y() << "]; radius = " << radius() << "; height = " << height() << "\n"; }

  protected:
    double m_height;
};
