#pragma once

#include <string>
using std::string;
#include <iostream>
using std::cout;

#include "circular.h"

class Circle : public Circular<Circle>
{
  public:
    Circle(const double radius = 1, const int x = 0, const int y = 0) : Circular(radius, x, y) {}

    string name() const { return "Circle"; }
    void print() const { cout << "Circle: [" << x() << ", " << y() << "]; radius = " << radius() << "\n"; }

};
