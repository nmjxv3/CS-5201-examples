#include <iostream>
using namespace std;

#include "shape.h"
#include "circular.h"

#include "point.h"
#include "circle.h"
#include "cylinder.h"

template<class Derived>
void printShape(const Shape<Derived>& shape)
{
  shape.print();
  cout << "This " << shape.name() << " has area " << shape.area() << ".\n";
  cout << "This " << shape.name() << " has volume " << shape.volume() << ".\n\n";
}

template<class Derived>
void printCircular(const Circular<Derived>& circ)
{
  cout << "This round " << circ.name() << " has nonzero area " << circ.area() << "\n";

  printShape(circ);
}


int main()
{
  Point point(7,11);
  Circle circle(3.5, 22, 8);
  Cylinder cylinder(10, 3.3, 10, 10);

  printShape(point);
  printShape(circle);
  printShape(cylinder);

  //printCircular(point);
  printCircular(circle);
  printCircular(cylinder);


  return 0;
}

/* OUTPUT:
Point: [7, 11]
This Point has area 0.
This Point has volume 0.

Circle: [22, 8]; radius = 3.5
This Circle has area 38.465.
This Circle has volume 0.

Cylinder: [10, 10]; radius = 3.3; height = 10
This Cylinder has area 34.1946.
This Cylinder has volume 341.946.

This round Circle has nonzero area 38.465
Circle: [22, 8]; radius = 3.5
This Circle has area 38.465.
This Circle has volume 0.

This round Cylinder has nonzero area 34.1946
Cylinder: [10, 10]; radius = 3.3; height = 10
This Cylinder has area 34.1946.
This Cylinder has volume 341.946.
*/
