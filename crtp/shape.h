#pragma once

#include <string>
using std::string;

template<class Derived>
class Shape
{
  public:
    Shape(const int x = 0, const int y = 0) : m_x(x), m_y(y) {}

    // Interface
    double area() const { return asDerived().area_impl(); }
    double volume() const { return asDerived().volume_impl(); }

    void setPoint(const int x = 0, const int y = 0) { return asDerived().setPoint_impl(x,y); }
    int x() const { return asDerived().x_impl(); }
    int y() const { return asDerived().y_impl(); }


    string name() const { return asDerived().name(); }
    void print() const { asDerived().print(); }



    // Implementation
    double area_impl() const { return 0; }
    double volume_impl() const { return 0; }

    void setPoint_impl(const int x = 0, const int y = 0)
    {
      m_x = x;
      m_y = y;
    }

    int x_impl() const { return m_x; }
    int y_impl() const { return m_y; }



    // CRTP casts
    Derived& asDerived() { return static_cast<Derived&>(*this); }
    const Derived& asDerived() const { return static_cast<const Derived&>(*this); }

  protected:
    int m_x;
    int m_y;
};
