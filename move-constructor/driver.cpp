#include "array.h"

#include<iostream>
using std::cout;
using std::endl;

int main() {
  Array stuff(10);
  for(size_t i = 0; i < stuff.size(); i++) {
    stuff[i] = i;
  }

  Array should_move = stuff + 5;
  cout << should_move << endl;
  return 0;
}
