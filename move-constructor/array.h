#pragma once
#include <stdexcept>
using std::out_of_range;
#include<iostream>
using std::ostream;

class Array {
  private:
    size_t _size;
    int* _data;

  public:
    // Constructors
    Array(size_t size) : _size(size), _data(new int[_size]) {}

    Array(const Array& a) : _size(a.size()), _data(new int[_size]) {
      for(size_t i = 0; i < a.size(); i++) {
        _data[i] = a[i];
      }
    }

    Array(Array&& a) : _size(0), _data(nullptr) {
      swap(*this, a);
    }


    // Destructor
    ~Array() {
      delete [] _data;
    }


    // Assignment
    Array& operator=(Array a) {
      if(a.size() != size()) {
        delete [] _data;
        _data = new int[a.size()];
        _size = a.size();
      }

      for(size_t i = 0; i < size(); i++) {
        _data[i] = a[i];
      }

      return *this;
    }

    friend void swap(Array& l, Array& r) {
      std::swap(l._size, r._size);
      std::swap(l._data, r._data);
    }


    // Accessors
    int& operator[](size_t idx) {
      if(idx >= _size) {
        throw out_of_range("Array::operator[]");
      }

      return _data[idx];
    }

    int operator[](size_t idx) const {
      if(idx >= _size) {
        throw out_of_range("Array::operator[]");
      }

      return _data[idx];
    }

    size_t size() const {
      return _size;
    }


    // Addition
    Array& operator+=(const int val) {
      for(size_t i = 0; i < size(); i++) {
        _data[i] += val;
      }

      return *this;
    }
};


// Non-member operators
Array operator+(Array a, const int val) {
  return a += val;
}


ostream& operator<<(ostream& out, const Array& a) {
  out << "[";

  if(a.size() > 0) {
    for(size_t i = 0; i < a.size() - 1; i++) {
      out << a[i] << ", ";
    }
    out << a[a.size() - 1];
  }

  out << "]";
  return out;
}