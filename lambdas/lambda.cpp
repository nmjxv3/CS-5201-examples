/* An example demonstrating how lambdas can be used to "glue" together different
 * functions that don't quite match up.
 *
 * This program takes a command line argument that is a file of vectors,
 * then prompts the user to either normalize every vector in the file or
 * multiply each vector by a specific scalar.
 *
 * We abstract all the functionality of reading the vectors from the file
 * and doing some 'stuff' to them into the `do_stuff_to_vectors` function.
 *
 * The lambdas come in because `do_stuff_to_vectors` expects a function that
 * takes one argument, a Vector<float>. However, performing a scalar product
 * requires two arguments! Thus, we can use a lambda to 'wrap up' the scalar
 * product in a function that behaves the way `do_stuff_to_vectors` expects.
 */




#include <iostream>
#include <fstream>
#include <vector>
#include "Vector.h"

using namespace std;
using namespace math;               // I put my Vector in the 'math' namespace




template<class Func>
void do_stuff_to_vectors(const char* filename, Func stuff)
{
  ifstream in(filename);         // Open the file and read in a bunch of vectors

  int vec_size;
  in >> vec_size;


  // I apologize for the ambiguity between vector and Vector

  vector<Vector<float>> vectors;

  Vector<float> temp(vec_size);
  while(in >> temp)
    vectors.push_back(temp);

  in.close();




  for(auto vec : vectors)        // Using a range-based for loop!
  {

    // Print out each vector and the result of calling 'stuff' on it

    cout << vec << ": " << stuff(vec) << endl;
  }
}









int main(int argc, char** argv)
{
  if(argc != 2)
  {
    cerr << "Too many or too few args; Goldilocks is disappointed in you." << endl;
    return 1;
  }

  char* vectors_file = argv[1];

  char reply;
  cout << "Normalize (n) or Scalar Product (s)? ";
  cin >>  reply;



  if(reply == 'n')
  {

    // When normalizing, all we want is a function that takes a Vector<float> and
    // calls normalize on it...pretty simple

    do_stuff_to_vectors(vectors_file,
        [](Vector<float> vec) { return vec.normalize(); }
      );

  }
  else if(reply == 's')
  {
    float scalar;
    cout << "What scalar you wanna use? ";
    cin >> scalar;


    // When multiplying by a scalar, we need to package up the scalar to multiply by
    // This is where capturing comes in handy!

    do_stuff_to_vectors(vectors_file,
        [=](Vector<float> vec) { return vec * scalar; }
      );

  }
  else
  {
    cout << "It's ok, I can't read good either." << endl;
  }

  cout << "Have a nice day!" << endl;
  return 0;
}

/* Output:

$ ./lambda input_files/4vect.txt
Normalize (n) or Scalar Product (s)? n
3, 6, 8, 0: 0.287348, 0.574696, 0.766261, 0
1, 2, 3, 4: 0.182574, 0.365148, 0.547723, 0.730297
4, 6, 8, 9: 0.284988, 0.427482, 0.569976, 0.641223
2, 0, 5, 7: 0.226455, 0, 0.566139, 0.792594
Have a nice day!
$ ./lambda input_files/4vect.txt
Normalize (n) or Scalar Product (s)? s
What scalar you wanna use? -0.2
3, 6, 8, 0: -0.6, -1.2, -1.6, -0
1, 2, 3, 4: -0.2, -0.4, -0.6, -0.8
4, 6, 8, 9: -0.8, -1.2, -1.6, -1.8
2, 0, 5, 7: -0.4, -0, -1, -1.4
Have a nice day!
*/
