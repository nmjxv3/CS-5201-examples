/* An event system using callbacks and lambdas
 *
 * There are two kinds of events: getting a line of input, and quitting the program.
 * Objects can register callbacks for either of these events. Each registered callback
 * is assigned a handle (in this case, a number) and stored in a std::map with the key
 * being the callback handle. The handle can be used to unregister a callback later.
 *
 * The classes listening for events are 
 * - Printer, which echos the text back to stdout
 * - Quitter, which exits the program when "quit" is typed
 * - Logger, which counts input lines and writes them to a file
 *
 */

#include<fstream>
#include<functional>
#include<iostream>
#include<map>
#include<string>
using namespace std;


class EventDispatcher {
	public:
		EventDispatcher() : quitting(false), next_handle(0) {}

		// EVENTS

		// Returns true if events should continue to be processed
		bool run_loop() {

			// Quit event
			if(quitting) {
				// run quit callbacks
				for(auto& [handle, cb] : quit_callbacks) {
					cb(*this);
				}
				return false;
			}

			// Input event
			else {
				string input;
				getline(cin, input);

				// run input callbacks
				for(auto& [handle, cb] : input_callbacks) {
					cb(*this, input);
				}
				return true;
			}
		}

		// A way for other objects to send a 'quit' event
		void quit() {
			quitting = true;	
		}














		// CALLBACKS

		// Each callback needs a unique handle so they can be unregistered
		using callback_handle = unsigned int;


		// Input callbacks get the input line and the ability to generate new events
		using input_callback = function<void(EventDispatcher&, const string&)>;

		callback_handle register_input_callback(input_callback f) {
			input_callbacks.insert(pair(next_handle, f));
			return next_handle++;
		}


		// Quit callbacks get the ability to generate new events
		using quit_callback = function<void(EventDispatcher&)>;

		callback_handle register_quit_callback(quit_callback f) {
			quit_callbacks.insert(pair(next_handle, f));
			return next_handle++;
		}


	  // Given a handle, unregister a callback
		void unregister_callback(const callback_handle h) {
			input_callbacks.erase(h);
			quit_callbacks.erase(h);
		}

	private:
		bool quitting;
		callback_handle next_handle;

		map<callback_handle, input_callback> input_callbacks;
		map<callback_handle, quit_callback> quit_callbacks;
};


// Print data to the given ostream
class Printer {
	public:
		Printer(EventDispatcher& ev, ostream& os) {
			ev.register_input_callback([&] (EventDispatcher&, const string& input) {
				os << input << endl;
			});
		}
};


// Quit if the line "quit" is read
class Quitter {
	public:
		Quitter(EventDispatcher& ev) {
			ev.register_input_callback([] (EventDispatcher& ev, const string& input) {
				if(input == "quit") {
				  ev.quit();
				}
			});
		}
};






// Log input lines to a file and count how many lines are logged
class Logger {
	public:
		Logger(EventDispatcher& ev, const string& filename) : num_input_lines(0), ev(ev) {
			out.open(filename);

			input_handle = ev.register_input_callback(
				[&] (EventDispatcher&, const string& input) {
					log_line(input);
				}
			);

			quit_handle = ev.register_quit_callback([&] (EventDispatcher&) {
				quit();
			});
		}

		// Since the lambdas capture by reference, when this object is destroyed
		// we MUST prevent the callbacks from being called!
		~Logger() {
			ev.unregister_callback(input_handle);
			ev.unregister_callback(quit_handle);
		}

		void log_line(const string& input) {
			num_input_lines++;
			out << input << endl;
		}

		void quit() {
			cout << "Logged " << num_input_lines << " lines of input!" << endl;
		}

	private:
		int num_input_lines;
		ofstream out;

		EventDispatcher::callback_handle input_handle, quit_handle;
		EventDispatcher& ev;
};



int main() {
	// Make the event dispatcher
	EventDispatcher ev;

	// Set up the objects listening for events
	Printer p(ev, cout);
	Quitter q(ev);
	Logger  l(ev, "logfile");

	// Handle events
	while(ev.run_loop()) {
		// Do any inter-event processing
	}

	return 0;
}
