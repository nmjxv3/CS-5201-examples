#pragma once

template<class T>
struct epsilon
{
  constexpr static T value = 0;
};

template<>
struct epsilon<double>
{
  constexpr static double value = 1e-8;
};

template<>
struct epsilon<float>
{
  constexpr static float value = 1e-6f;
};

