#include<vector>
#include<iostream>
#include<rapidcheck.h>
using namespace std;


// Property-based testing: test properties ("invariants") of your code
// with randomly generated examples!
//
// Rapidcheck docs here: https://github.com/emil-e/rapidcheck/blob/master/doc/user_guide.md
// Generating instances of your own classes: https://github.com/emil-e/rapidcheck/blob/master/doc/generators.md


// Insertion sort!
template<class T>
void sort(vector<T>& v) {
	for (auto it = v.begin(); it != v.end(); it++) {
		// Find where in the sorted array *it goes
		for (auto replace = v.begin(); replace < it; replace++) {
			if (*it < *replace) {
				// Move all the elements down one; use it as temporary storage
				for (; replace < it; replace++) {
					swap(*it, *replace);
				}
			}
		}
	}
}

// Check if a vector is indeed sorted
template<class T>
bool is_sorted(const vector<T>& v) {
	bool sorted = true;
	for(auto it = v.begin() + 1; it < v.end(); it++) {
		sorted &= (*(it - 1) <= *it);
	}
	
	return sorted;
}


// Check if a vector is a palindrome
template<class T>
bool symmetric(const vector<T>& v) {
	bool symmetric = true;
 for (auto f = v.begin(), b = v.end(); f < b; f++, b--) {
		symmetric &= (*f == *(b - 1));
	}

	return symmetric;
}

// Reverse a vector
template<class T>
void reverse(vector<T>& v) {
 // broken:
 // for (auto f = v.begin(), b = v.rbegin(); f != v.end(); f++, b++) {
 for (auto f = v.begin(), b = v.end(); f < b; f++, b--) {
		swap(*f, *(b - 1));
	}
}



// And here's the properties!

int main() {

	rc::check("Reversing twice is identity",
		[](vector<int> v) {
		  auto orig = v;
			reverse(v);
			reverse(v);
			RC_ASSERT(orig == v);
		});

  rc::check("Reversing once does something",
		[](vector<int> v) {
		 	// Reject lists that are palindromes
		  RC_PRE(!symmetric(v));

		  auto orig = v;
			reverse(v);
			RC_ASSERT(orig != v);
		});

	rc::check("Sorting produces a sorted vector",
	  [](vector<int> v) {
      sort(v);
	    RC_ASSERT(is_sorted(v));
		});

	rc::check("Sorting is idempotent",
		[](vector<int> v) {
		  sort(v);
			auto once = v;

			sort(v);

			RC_ASSERT(v == once);
		});

	return 0;
}
